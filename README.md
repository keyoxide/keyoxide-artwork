# keyoxide-artwork

Artwork and logos for the Keyoxide project

## Usage

Run `make` (or `make -B` on subsequent runs) to generate PNGs from the SVG files.

## License

Images licensed under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).