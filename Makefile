BUILD_DIR := ./build

FILES_IN := $(wildcard *.svg)
# FILES_IN := keyoxide.svg

FILES_OUT_PNG_SQUARE_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.square.1x.png))
FILES_OUT_PNG_SQUARE_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.square.2x.png))
FILES_OUT_PNG_SQUARE_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.square.3x.png))
FILES_OUT_PNG_SQUARE_4X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.square.4x.png))
FILES_OUT_PNG_SQUARE_5X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.square.5x.png))
FILES_OUT_PNG_SQUARE_6X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.square.6x.png))
FILES_OUT_PNG_SQUARE := $(FILES_OUT_PNG_SQUARE_1X) $(FILES_OUT_PNG_SQUARE_2X) $(FILES_OUT_PNG_SQUARE_3X) $(FILES_OUT_PNG_SQUARE_4X) $(FILES_OUT_PNG_SQUARE_5X) $(FILES_OUT_PNG_SQUARE_6X)

FILES_OUT_PNG_ROUNDED_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.rounded.1x.png))
FILES_OUT_PNG_ROUNDED_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.rounded.2x.png))
FILES_OUT_PNG_ROUNDED_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.rounded.3x.png))
FILES_OUT_PNG_ROUNDED_4X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.rounded.4x.png))
FILES_OUT_PNG_ROUNDED_5X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.rounded.5x.png))
FILES_OUT_PNG_ROUNDED_6X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.rounded.6x.png))
FILES_OUT_PNG_ROUNDED := $(FILES_OUT_PNG_ROUNDED_1X) $(FILES_OUT_PNG_ROUNDED_2X) $(FILES_OUT_PNG_ROUNDED_3X) $(FILES_OUT_PNG_ROUNDED_4X) $(FILES_OUT_PNG_ROUNDED_5X) $(FILES_OUT_PNG_ROUNDED_6X)

FILES_OUT_PNG_CIRCLE_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.circle.1x.png))
FILES_OUT_PNG_CIRCLE_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.circle.2x.png))
FILES_OUT_PNG_CIRCLE_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.circle.3x.png))
FILES_OUT_PNG_CIRCLE_4X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.circle.4x.png))
FILES_OUT_PNG_CIRCLE_5X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.circle.5x.png))
FILES_OUT_PNG_CIRCLE_6X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.circle.6x.png))
FILES_OUT_PNG_CIRCLE := $(FILES_OUT_PNG_CIRCLE_1X) $(FILES_OUT_PNG_CIRCLE_2X) $(FILES_OUT_PNG_CIRCLE_3X) $(FILES_OUT_PNG_CIRCLE_4X) $(FILES_OUT_PNG_CIRCLE_5X) $(FILES_OUT_PNG_CIRCLE_6X)

FILES_OUT_PNG_ADAPTIVE_ICON_FG_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_fg.1x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FG_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_fg.2x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FG_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_fg.3x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FG_4X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_fg.4x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FG_5X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_fg.5x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FG_6X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_fg.6x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FG := $(FILES_OUT_PNG_ADAPTIVE_ICON_FG_1X) $(FILES_OUT_PNG_ADAPTIVE_ICON_FG_2X) $(FILES_OUT_PNG_ADAPTIVE_ICON_FG_3X) $(FILES_OUT_PNG_ADAPTIVE_ICON_FG_4X) $(FILES_OUT_PNG_ADAPTIVE_ICON_FG_5X) $(FILES_OUT_PNG_ADAPTIVE_ICON_FG_6X)

FILES_OUT_PNG_ADAPTIVE_ICON_BG_1X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_bg.1x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_BG_2X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_bg.2x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_BG_3X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_bg.3x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_BG_4X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_bg.4x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_BG_5X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_bg.5x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_BG_6X := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_bg.6x.png))
FILES_OUT_PNG_ADAPTIVE_ICON_BG := $(FILES_OUT_PNG_ADAPTIVE_ICON_BG_1X) $(FILES_OUT_PNG_ADAPTIVE_ICON_BG_2X) $(FILES_OUT_PNG_ADAPTIVE_ICON_BG_3X) $(FILES_OUT_PNG_ADAPTIVE_ICON_BG_4X) $(FILES_OUT_PNG_ADAPTIVE_ICON_BG_5X) $(FILES_OUT_PNG_ADAPTIVE_ICON_BG_6X)

FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER_FG := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_flutter_fg.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER_BG := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_flutter_bg.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER_FULL := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.adaptive_icon_flutter_full.png))
FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER := $(FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER_FG) $(FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER_BG) $(FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER_FULL)

FILES_OUT_PNG_ADAPTIVE_ICON :=  $(FILES_OUT_PNG_ADAPTIVE_ICON_FG) $(FILES_OUT_PNG_ADAPTIVE_ICON_BG) $(FILES_OUT_PNG_ADAPTIVE_ICON_FLUTTER)

FILES_OUT_PNG_BANNER_1200_400 := $(addprefix $(BUILD_DIR)/, $(FILES_IN:.svg=.banner_1200_400.png))
FILES_OUT_PNG_OTHER :=  $(FILES_OUT_PNG_BANNER_1200_400)

FILES_OUT_PNG := $(FILES_OUT_PNG_SQUARE) $(FILES_OUT_PNG_ROUNDED) $(FILES_OUT_PNG_CIRCLE) $(FILES_OUT_PNG_ADAPTIVE_ICON) $(FILES_OUT_PNG_OTHER)

FILES_OUT := $(FILES_OUT_PNG)

.PHONY: all
all: $(FILES_OUT)

# PNG SQUARE 1X
$(BUILD_DIR)/%.square.1x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='2304,000:0,000:3328,000:1024,000' --export-filename='$@' \
		--export-width='48' --export-type='png' $<

# PNG SQUARE 2X
$(BUILD_DIR)/%.square.2x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='2304,000:0,000:3328,000:1024,000' --export-filename='$@' \
		--export-width='64' --export-type='png' $<

# PNG SQUARE 3X
$(BUILD_DIR)/%.square.3x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='2304,000:0,000:3328,000:1024,000' --export-filename='$@' \
		--export-width='128' --export-type='png' $<

# PNG SQUARE 4X
$(BUILD_DIR)/%.square.4x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='2304,000:0,000:3328,000:1024,000' --export-filename='$@' \
		--export-width='256' --export-type='png' $<

# PNG SQUARE 5X
$(BUILD_DIR)/%.square.5x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='2304,000:0,000:3328,000:1024,000' --export-filename='$@' \
		--export-width='512' --export-type='png' $<

# PNG SQUARE 6X
$(BUILD_DIR)/%.square.6x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='2304,000:0,000:3328,000:1024,000' --export-filename='$@' \
		--export-width='1024' --export-type='png' $<



# PNG ROUNDED 1X
$(BUILD_DIR)/%.rounded.1x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='1152,000:0,000:2176,000:1024,000' --export-filename='$@' \
		--export-width='48' --export-type='png' $<

# PNG ROUNDED 2X
$(BUILD_DIR)/%.rounded.2x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='1152,000:0,000:2176,000:1024,000' --export-filename='$@' \
		--export-width='64' --export-type='png' $<

# PNG ROUNDED 3X
$(BUILD_DIR)/%.rounded.3x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='1152,000:0,000:2176,000:1024,000' --export-filename='$@' \
		--export-width='128' --export-type='png' $<

# PNG ROUNDED 4X
$(BUILD_DIR)/%.rounded.4x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='1152,000:0,000:2176,000:1024,000' --export-filename='$@' \
		--export-width='256' --export-type='png' $<

# PNG ROUNDED 5X
$(BUILD_DIR)/%.rounded.5x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='1152,000:0,000:2176,000:1024,000' --export-filename='$@' \
		--export-width='512' --export-type='png' $<

# PNG ROUNDED 6X
$(BUILD_DIR)/%.rounded.6x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='1152,000:0,000:2176,000:1024,000' --export-filename='$@' \
		--export-width='1024' --export-type='png' $<



# PNG CIRCLE 1X
$(BUILD_DIR)/%.circle.1x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:0,000:1024,000:1024,000' --export-filename='$@' \
		--export-width='48' --export-type='png' $<

# PNG CIRCLE 2X
$(BUILD_DIR)/%.circle.2x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:0,000:1024,000:1024,000' --export-filename='$@' \
		--export-width='64' --export-type='png' $<

# PNG CIRCLE 3X
$(BUILD_DIR)/%.circle.3x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:0,000:1024,000:1024,000' --export-filename='$@' \
		--export-width='128' --export-type='png' $<

# PNG CIRCLE 4X
$(BUILD_DIR)/%.circle.4x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:0,000:1024,000:1024,000' --export-filename='$@' \
		--export-width='256' --export-type='png' $<

# PNG CIRCLE 5X
$(BUILD_DIR)/%.circle.5x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:0,000:1024,000:1024,000' --export-filename='$@' \
		--export-width='512' --export-type='png' $<

# PNG CIRCLE 6X
$(BUILD_DIR)/%.circle.6x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:0,000:1024,000:1024,000' --export-filename='$@' \
		--export-width='1024' --export-type='png' $<



# PNG ADAPTIVE ICON FG 1X
$(BUILD_DIR)/%.adaptive_icon_fg.1x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3550,154:0,000:3658,154:108,000' --export-filename='$@' \
		--export-dpi='72' --export-type='png' $<

# PNG ADAPTIVE ICON FG 2X
$(BUILD_DIR)/%.adaptive_icon_fg.2x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3550,154:0,000:3658,154:108,000' --export-filename='$@' \
		--export-dpi='96' --export-type='png' $<

# PNG ADAPTIVE ICON FG 3X
$(BUILD_DIR)/%.adaptive_icon_fg.3x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3550,154:0,000:3658,154:108,000' --export-filename='$@' \
		--export-dpi='144' --export-type='png' $<

# PNG ADAPTIVE ICON FG 4X
$(BUILD_DIR)/%.adaptive_icon_fg.4x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3550,154:0,000:3658,154:108,000' --export-filename='$@' \
		--export-dpi='288' --export-type='png' $<

# PNG ADAPTIVE ICON FG 5X
$(BUILD_DIR)/%.adaptive_icon_fg.5x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3550,154:0,000:3658,154:108,000' --export-filename='$@' \
		--export-dpi='384' --export-type='png' $<

# PNG ADAPTIVE ICON FG 6X
$(BUILD_DIR)/%.adaptive_icon_fg.6x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3550,154:0,000:3658,154:108,000' --export-filename='$@' \
		--export-dpi='768' --export-type='png' $<

# PNG ADAPTIVE ICON FLUTTER FG
$(BUILD_DIR)/%.adaptive_icon_flutter_fg.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3686,773:0,000:3794,773:108,000' --export-filename='$@' \
		--export-width='1024' --export-type='png' $<



# PNG ADAPTIVE ICON BG 1X
$(BUILD_DIR)/%.adaptive_icon_bg.1x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-dpi='72' --export-type='png' $<

# PNG ADAPTIVE ICON BG 2X
$(BUILD_DIR)/%.adaptive_icon_bg.2x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-dpi='96' --export-type='png' $<

# PNG ADAPTIVE ICON BG 3X
$(BUILD_DIR)/%.adaptive_icon_bg.3x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-dpi='144' --export-type='png' $<

# PNG ADAPTIVE ICON BG 4X
$(BUILD_DIR)/%.adaptive_icon_bg.4x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-dpi='288' --export-type='png' $<

# PNG ADAPTIVE ICON BG 5X
$(BUILD_DIR)/%.adaptive_icon_bg.5x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-dpi='384' --export-type='png' $<

# PNG ADAPTIVE ICON BG 6X
$(BUILD_DIR)/%.adaptive_icon_bg.6x.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-dpi='768' --export-type='png' $<

# PNG ADAPTIVE ICON FLUTTER BG
$(BUILD_DIR)/%.adaptive_icon_flutter_bg.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='3417,188:0,000:3525,188:108,000' --export-filename='$@' \
		--export-width='1024' --export-type='png' $<



# PNG ADAPTIVE ICON FLUTTER FULL
$(BUILD_DIR)/%.adaptive_icon_flutter_full.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@convert -composite ./build/keyoxide.adaptive_icon_flutter_bg.png ./build/keyoxide.adaptive_icon_flutter_fg.png -depth 8 $@



# PNG BANNER 1200x400
$(BUILD_DIR)/%.banner_1200_400.png: %.svg | $(BUILD_DIR)
	@echo "Generating $@"
	@inkscape --export-area='0,000:2500,000:2400,000:3300,00' --export-filename='$@' \
		--export-width='1200' --export-type='png' $<



$(BUILD_DIR):
	@mkdir -p $@